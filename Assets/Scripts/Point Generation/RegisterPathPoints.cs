﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
public class RegisterPathPoints : MonoBehaviour
{
    public GameObject _paths;
    public void AssignPossiblePaths()
    {
        LayerMask midPointlayer = LayerMask.NameToLayer("Path_MidPoint");
        LayerMask startPointlayer = LayerMask.NameToLayer("Path_Start");

        foreach (Transform child in _paths.transform)
        {
            if (child.gameObject.layer == startPointlayer) child.gameObject.AddComponent<StartPoints>();
            else if (child.gameObject.layer == midPointlayer) child.gameObject.AddComponent<MidPoints>();
        }
        foreach (Transform child in _paths.transform)
        {
            if (child.gameObject.layer == startPointlayer)
            {
                child.gameObject.GetComponent<StartPoints>()._firstMidPoint = StartPoints.AssignTargetMidPoint(child.gameObject);
                child.gameObject.GetComponent<StartPoints>()._firstMidPoint.GetComponent<MidPoints>().StartPointDirectlyBehindMe = child.gameObject;
            }
        }
        foreach (Transform child in _paths.transform)
        {
            if (child.gameObject.layer == midPointlayer)
            {
                child.gameObject.GetComponent<MidPoints>()._targetStartPoints = MidPoints.StartPointsInRange(child.gameObject.transform, child.gameObject.GetComponent<MidPoints>().StartPointDirectlyBehindMe, child.gameObject.tag);
            }
        }
        foreach (Transform child in _paths.transform)
        {
            if (child.gameObject.layer == midPointlayer)
            {
                child.gameObject.GetComponent<MidPoints>().startTargets = MidPoints.GenerateTurns(child.gameObject, child.gameObject.GetComponent<MidPoints>()._targetStartPoints);
            }
        }
    }
}
