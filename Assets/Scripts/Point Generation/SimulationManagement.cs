﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SimulationManagement : MonoBehaviour
{
    public bool _saveAssets = false;
    //public ConfigurationCarSpawn _carSpawner;
    public RegisterPathPoints _pointRegister;
    public GameObject spawnPoints;
    private void Awake()
    {
        _pointRegister.AssignPossiblePaths();
        //_carSpawner.SpawnTheCars();
        if (_saveAssets)
        {
            GameObject ReadyPaths = _pointRegister._paths;
            string localPath = "Assets/" + ReadyPaths.name + ".prefab";
            localPath = AssetDatabase.GenerateUniqueAssetPath(localPath);
            PrefabUtility.SaveAsPrefabAsset(ReadyPaths, localPath);
            GameObject SpawnPoints = spawnPoints;
            string localPath1 = "Assets/" + SpawnPoints.name + ".prefab";
            localPath1 = AssetDatabase.GenerateUniqueAssetPath(localPath1);
            PrefabUtility.SaveAsPrefabAsset(SpawnPoints, localPath1);
        }
    }
}
