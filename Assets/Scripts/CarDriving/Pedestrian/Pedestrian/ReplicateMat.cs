﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReplicateMat : MonoBehaviour
{
    void Update()
    {
        transform.GetComponent<SkinnedMeshRenderer>().enabled = transform.parent.GetComponent<SkinnedMeshRenderer>().enabled;
        transform.GetComponent<SkinnedMeshRenderer>().material.shader = transform.parent.GetComponent<SkinnedMeshRenderer>().material.shader;

    }
}
