﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor.Animations;
using UnityEngine;
using UnityEngine.AI;

public class NPC_behaviour : MonoBehaviour
{
    [Header("Variable to make him stop for a desired duration to each target. (s)")]
    public float _durationToWait = 2f;
    [Header("When we should consider that the agent has reached his destination")]
    public float _distMeterLimit = 0.1f;
    [HideInInspector]
    public Transform[] _destinations;
    private Transform _currentDestination;
    private MovementCycle _currentMovementState;
    NavMeshAgent _navMeshAgent;
    Animator _animator;
    private enum MovementCycle { JustStarted,Stop,MovingTowardsTarget,ReachedTarget}
    private readonly float _movingSpeed = 0.5f;
    void Start()
    {
        _animator = GetComponent<Animator>();
        _navMeshAgent = GetComponent<NavMeshAgent>();
        _navMeshAgent.speed = _movingSpeed;

        _currentMovementState = MovementCycle.JustStarted;
    }
    private void Update()
    {
        UpdateNavMeshAgent(ref _navMeshAgent); //Update Destination of NavMesh Agent and whether he should stop.
        UpdatePedestrianMovementStatus(ref _currentMovementState, _navMeshAgent, _distMeterLimit); //Update whether we've reached to our destination or we should keep walking
        UpdateAnimator(ref _animator,_navMeshAgent); //Control the variables responsible for changing the animations between walking and idle.
    }

    private void UpdateNavMeshAgent(ref NavMeshAgent agent)
    {
        agent.SetDestination(UpdateDestination(ref _currentDestination, _currentMovementState, _destinations));
        UpdateNavMeshAgentStoppingStatus(_currentMovementState, _durationToWait);

    }

    private Vector3 UpdateDestination(ref Transform _currentDestination, MovementCycle movementStatus, Transform[] destinations) //Iterate through all the destinations and when you reach the end start oer.
    {
        switch (movementStatus)
        {
            case MovementCycle.JustStarted:
                _currentDestination = destinations[0];
                break;
            case MovementCycle.ReachedTarget:
                int indexOfDestInArray = Array.IndexOf(destinations, _currentDestination);
                if (indexOfDestInArray < destinations.Length - 1) _currentDestination = destinations[indexOfDestInArray += 1];//Keep iterating
                else _currentDestination = destinations[0];//Start over
                break;
            case MovementCycle.MovingTowardsTarget:
                break;
            default:
                Debug.Log("Error! Problem with available Movement Status");
                break;
        }
        return _currentDestination.position;
    }
    private void UpdateNavMeshAgentStoppingStatus(MovementCycle status, float timeInterval)
    {
        if (status==MovementCycle.ReachedTarget) StartCoroutine(TimeCount(timeInterval));
    }
    private void UpdatePedestrianMovementStatus(ref MovementCycle movevementStatus, NavMeshAgent agent, float limitDist)
    {

        if (agent.remainingDistance <= limitDist) movevementStatus = MovementCycle.ReachedTarget;
        else movevementStatus = MovementCycle.MovingTowardsTarget;
    }
    private void UpdateAnimator(ref Animator animator, NavMeshAgent agent)
    {
        animator.SetBool("Stopped",agent.isStopped);
    }
    private IEnumerator TimeCount(float timeInterval)
    {
        _navMeshAgent.isStopped = true;
        yield return new WaitForSeconds(timeInterval);
        _navMeshAgent.isStopped = false;
    }
}
