﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class IntegratePedestriansAndDestinations : MonoBehaviour
{
    public GameObject Pedestrian;
    public Transform _walkingPaths;
    private List<Transform> _individualDestinations=new List<Transform>();
    private void Awake()
    {
        foreach (Transform child in _walkingPaths)
        {
            foreach (Transform grandChild in child)
            {
                _individualDestinations.Add(grandChild);
            }
            GameObject Ped = Instantiate(Pedestrian, _individualDestinations[_individualDestinations.Count - 1].position,Quaternion.identity);
            Ped.GetComponent<NPC_behaviour>()._destinations = _individualDestinations.ToArray();
        }


    }
}
