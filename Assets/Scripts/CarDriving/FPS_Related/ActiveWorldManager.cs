﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveWorldManager : MonoBehaviour
{
    public GameObject _carPrefab;
    public GameObject _pathPointsRoot;

    private GameObject[] pathPoints;

    private LayerMask spawnPointLayer;
    private LayerMask carLayer;
    private LayerMask pedestrianLayer;


    private Color[] colorArray;
    private void Awake()
    {
        colorArray = new Color[5];
        colorArray[0] = Color.red;
        colorArray[1] = Color.blue;
        colorArray[2] = Color.black;
        colorArray[3] = Color.magenta;
        colorArray[4] = Color.black;

        spawnPointLayer = LayerMask.NameToLayer("SpawnPoint");
        carLayer= LayerMask.NameToLayer("Car");
        pedestrianLayer = LayerMask.NameToLayer("Pedestrian");
        pathPoints = new GameObject[_pathPointsRoot.transform.childCount];

        int i=0;
        foreach (Transform child in _pathPointsRoot.transform)
        {
            pathPoints[i] = child.gameObject;
            i++;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == spawnPointLayer)
        {
            GameObject nearestStartPoint = Helper.FindClosestGameObject(pathPoints, other.gameObject, "Path_Start");
            GameObject car = Instantiate(_carPrefab, other.gameObject.transform.position, Quaternion.identity);
            car.transform.LookAt(nearestStartPoint.transform);
            car.GetComponent<Data>().bodyPaintMat.material.color = colorArray[UnityEngine.Random.Range(0,colorArray.Length)];
            car.GetComponent<Data>().CurrentLane = other.gameObject.tag;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == carLayer || other.gameObject.layer==pedestrianLayer) Destroy(other.gameObject);
    }
}
