﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class NodeConfigurationForEachCar
{
    #region Attributes
    private static readonly float ThresholdToAcceptNewNodes = 10f;
    #endregion

    #region Public Methods
    public static void InitialConfiguration(ref GameObject Target, GameObject Car, string Lane,AgentSimulationEnum agentStatus)
    {
        if (agentStatus == AgentSimulationEnum.JustSpawned) Target = Helper.FindNearestGameObjectFromSphereCollider(Car, Lane, "Path_Start");
    }
    public static void QuestionNewNode(ref GameObject Target, ref AgentSimulationEnum agentStatus,ref bool canChangeLane, ref bool Turning,ref List<Vector3> splinePoints, GameObject Car,ref bool PseudoTurning)
    {
        if(Car.transform.InverseTransformPoint(Target.transform.position).magnitude<ThresholdToAcceptNewNodes)
        {
            switch (agentStatus)
            {
                case AgentSimulationEnum.JustSpawned:
                    Target = /*Helper.SelectRandomAvailablePoint(Target.GetComponent<StartPoints>()._targetMidPoints)*/Target.GetComponent<StartPoints>()._firstMidPoint;
                    agentStatus = AgentSimulationEnum.MovingTowardsMidPoint;
                    break;
                case AgentSimulationEnum.MovingTowardsStart:
                    Target = /*Helper.SelectRandomAvailablePoint(Target.GetComponent<StartPoints>()._targetMidPoints)*/Target.GetComponent<StartPoints>()._firstMidPoint;
                    agentStatus = AgentSimulationEnum.MovingTowardsMidPoint;
                    break;
                case AgentSimulationEnum.MovingTowardsMidPoint:
                    canChangeLane = false;
                    Turning = true;
                    GameObject nextStartPoint = Helper.SelectRandomAvailablePoint(Target.GetComponent<MidPoints>()._targetStartPoints);
                    int i = System.Array.IndexOf(Target.GetComponent<MidPoints>()._targetStartPoints, nextStartPoint);
                    splinePoints = Target.GetComponent<MidPoints>().startTargets[i].points.ToList();
                    PseudoTurning = Target.GetComponent<MidPoints>().startTargets[i].shortTurn;
                    Target = nextStartPoint;
                    agentStatus = AgentSimulationEnum.MovingTowardsStart;
                    break;
                default:
                    Debug.Log("Unknown Agent Status !!!");
                    break;
            }
        }
    }
    #endregion
}
