﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SensorsForOtherCars
{
    #region Attributes
    public static float _sensorLength=20f;
    private readonly static float[] _sensorAngles = { 0f, 10f, 20f, 30f, 40f, 50f, 60f, 70f, 80f, 90f };
    private readonly static int carLayer = LayerMask.GetMask("Car");
    #endregion

    #region Public Methods
    public static void UpdateCarsAndDistances(ref GameObject frontSideCar,ref float frontSideCarDistance, ref GameObject rightSideCar, ref float rightSideCarDistance, ref GameObject leftSideCar, ref float leftSideCarDistance, Transform driverReference, string sideLane)
    {
        FrontSide(ref frontSideCar, ref frontSideCarDistance, driverReference, _sensorLength,_sensorAngles);
        LeftRightSide(ref rightSideCar, ref rightSideCarDistance,ref leftSideCar,ref leftSideCarDistance, driverReference, _sensorLength, sideLane,_sensorAngles);
        //DrawRays(driverReference.transform, _sensorLength, _sensorAngles);
    }
    #endregion

    #region Private Methods

    #region Front Side
    private static void FrontSide(ref GameObject frontSideCar, ref float frontSideCarDistance, Transform driverReference, float SensorLength,float[] Angles)
    {
        UpdateFrontSideCar(ref frontSideCar, driverReference, SensorLength,Angles);
        UpdateFrontSideCarDistance(ref frontSideCarDistance, driverReference, frontSideCar);
    }
    private static void UpdateFrontSideCar(ref GameObject FrontSideCar, Transform ReferenceTransform, float SensorLength,float[] Angles)
    {
        for (int i = 0; i < 2; i++)
        {
            if (Physics.Raycast(ReferenceTransform.position, Quaternion.AngleAxis(Angles[i], ReferenceTransform.up) * ReferenceTransform.forward, out RaycastHit hit, SensorLength,carLayer)) FrontSideCar = hit.collider.gameObject;
            else FrontSideCar = null;
        }
    }
    private static  void UpdateFrontSideCarDistance(ref float distanceFrontSideCar, Transform ReferenceTransform, GameObject frontSideCar)
    {
        if (frontSideCar != null) distanceFrontSideCar = ReferenceTransform.InverseTransformPoint(frontSideCar.transform.position).magnitude;
        else distanceFrontSideCar = 0f;
    }
    #endregion

    #region Left/Right Side
    private static void LeftRightSide(ref GameObject rightSideCar, ref float rightSideCarDistance, ref GameObject leftSideCar, ref float leftSideCarDistance,Transform driverReference, float sensorSideLen, string sideLane,float[] Angles)
    {
        UpdateSideCars(ref rightSideCar, ref leftSideCar, driverReference, sideLane, sensorSideLen,Angles);
        UpdateSideCarDistances(ref rightSideCarDistance,ref leftSideCarDistance, driverReference, rightSideCar, leftSideCar);
    }
    private static void UpdateSideCars(ref GameObject rightSideCar, ref GameObject leftSideCar, Transform Reference,String SideLane, float sensorSideLength,float[] Angles)
    {
        for (int i = 2; i < Angles.Length; i++)
        {
            RaycastHit hit;
            //Right
            if (Physics.Raycast(Reference.position, Quaternion.AngleAxis(Angles[i], Reference.up) * Reference.forward, out hit, sensorSideLength, carLayer))
            {
                if (hit.collider.gameObject.CompareTag("Car") && hit.collider.gameObject.GetComponent<Data>().CurrentLane==SideLane) rightSideCar = hit.collider.gameObject;
            }
            else rightSideCar = null;
            //Left
            if (Physics.Raycast(Reference.position, Quaternion.AngleAxis(-Angles[i], Reference.up) * Reference.forward, out hit, sensorSideLength, carLayer))
            {
                if (hit.collider.gameObject.CompareTag("Car") && hit.collider.gameObject.GetComponent<Data>().CurrentLane == SideLane) leftSideCar = hit.collider.gameObject;
            }
            else leftSideCar = null;
        }
    }
    private static void UpdateSideCarDistances(ref float distanceLeftSideCar, ref float distanceRightSideCar, Transform Reference, GameObject rightSideCar, GameObject leftSideCar)
    {
        if (leftSideCar != null) distanceLeftSideCar = Reference.InverseTransformPoint(leftSideCar.transform.position).magnitude;
        else distanceLeftSideCar = 0f;

        if (rightSideCar != null) distanceRightSideCar = Reference.InverseTransformPoint(rightSideCar.transform.position).magnitude;
        else distanceRightSideCar = 0f;
    }
    #endregion

    #endregion

    #region Debug
    private static void DrawRays(Transform Reference, float sensorSideLength, float[] Angles)
    {
        for (int i = 0; i < Angles.Length; i++)
        {
            Debug.DrawRay(Reference.position, Quaternion.AngleAxis(Angles[i], Reference.up) * Reference.forward * sensorSideLength, Color.green);
            Debug.DrawRay(Reference.position, Quaternion.AngleAxis(-Angles[i], Reference.up) * Reference.forward * sensorSideLength, Color.green);
        }
    }
    #endregion
}