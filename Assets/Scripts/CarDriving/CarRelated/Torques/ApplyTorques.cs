﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public static class ApplyTorques
{
    private static readonly float maxTorque = 100f;
    private static readonly float maxBreakTorque = 5000f;
    #region Public Methods
    public static void Drive(ref float motorTorque, ref float breakTorque, bool GO, bool STOP, bool Auto, bool Reverse, bool justRoll, bool usingSteeringWheel,int gasPedal, int breakPedal)
    {
        if (usingSteeringWheel && !Auto)
        {
            motorTorque = gasPedal;
            breakTorque = breakPedal;
        }
        else
        {
            if (GO)
            {
                motorTorque = Auto ? maxTorque : maxTorque*Mathf.Clamp(Input.GetAxis("Vertical"),-1,1);
                breakTorque = 0;
            }
            else if (STOP)
            {
                motorTorque = 0;
                breakTorque = Auto ? maxBreakTorque*0.01f:maxBreakTorque;
            }
            else if (!Auto && Reverse)
            {
                motorTorque = maxTorque * Mathf.Clamp(Input.GetAxis("Vertical"), -1, 1) /* mainScript.Break_Reverse_Pedal*/;
                breakTorque = 0;
            }
            else if (!Auto && justRoll)
            {
                motorTorque = 0;
                breakTorque = 0;
            }
        }
    }
    #endregion
}
