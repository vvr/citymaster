﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TurningBehaviour
{
    private static readonly float distBetweenPoints = 8f;
    #region Turn Methods
    /*
     * While turning, check each point of the CatMull Rom
     */
    public static void CheckPoints(ref bool Turning,ref int currentIndex,ref bool SlowDown,GameObject Reference,List<Vector3> splinePoints)
    {
        if (Turning)
        {
            if ((Reference.transform.InverseTransformPoint(splinePoints[currentIndex].x, Reference.transform.position.y, splinePoints[currentIndex].z)).magnitude < distBetweenPoints)
            {
                //Still in the Catmull Rom
                if (currentIndex < splinePoints.Count - 1)
                {
                    SlowDown = currentIndex < splinePoints.Count * (3 / 4f);
                    currentIndex++;
                }
                //We out
                else
                {
                    currentIndex = 0;
                    Turning = false;
                }
            }
        }
    }
    #endregion
}
