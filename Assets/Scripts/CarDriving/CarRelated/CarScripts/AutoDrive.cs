﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class AutoDrive
{
    #region Public Methods
    public static void DrivingDecision(ref bool STOP,ref bool GO,bool exceedingMaxSpeed,bool carInFrontOfMe,bool sideCarWantsToTurn,bool redLight,bool stopSign)
    {
        /*
         * If I'm at stop and there is a car in the lane I need to stop whatever the cost.
         * If There is a car in front of me over a specified limit I need to stop whatever the cost.
         * If I'm exceeding car's "Top" velocity I need to stop whatever the cost.
         */
        STOP = exceedingMaxSpeed || carInFrontOfMe|| sideCarWantsToTurn||redLight||stopSign;
        GO = (!STOP);
    }
    #endregion
}

