﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimateSteeringWheel : MonoBehaviour
{
    public Data Car;
    public WheelCollider wheel;
    float timer = 0f;
    float prevSteer = 0f;
    float pos = 0;
    void Update()
    {
        if (!Car.usingSteeringWheel)
        {
            if (timer > 1f) timer = 0f;
            timer += Time.deltaTime;
            float appliedSteer = Mathf.LerpAngle(prevSteer, -wheel.steerAngle, timer);
            transform.localRotation = Quaternion.Euler(15, 0, appliedSteer);
            prevSteer = appliedSteer;
        }
        else
        {
            LogitechGSDK.DIJOYSTATE2ENGINES rec = LogitechGSDK.LogiGetStateUnity(0);
            float temp0 = (rec.lX / 32750f) * 420f;
            transform.localRotation = Quaternion.Euler(15, 0, -temp0);
        }
    }
}
