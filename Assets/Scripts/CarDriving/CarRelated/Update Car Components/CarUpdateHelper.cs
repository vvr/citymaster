﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class CarUpdateHelper
{
    public static void WhichIsMySideLane(ref string SideLane, string currentLane)
    {
        switch (currentLane)
        {
            case "Lane1":
                SideLane = "Lane2";
                break;
            case "Lane2":
                SideLane = "Lane1";
                break;
            case "Lane3":
                SideLane = "Lane4";
                break;
            case "Lane4":
                SideLane = "Lane3";
                break;
            default:
                Debug.Log("Error! Unknown Lane");
                break;
        }
    }
    public static void UpdateVehicleSpeed(ref float currentSpeed, GameObject car)
    {
        currentSpeed= car.GetComponent<Rigidbody>().velocity.magnitude * 3.6f;
    }
}
