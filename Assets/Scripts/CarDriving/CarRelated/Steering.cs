﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Steering
{
    #region Attributes
    [Header("Steering Configurations")]
    public static float targetSteerAngle;
    public static float maxSteerAngle =30f;
    private static readonly float AntiRoll = 5000.0f;

    private static WheelHit hit;
    private static float travelL = 1f;
    private static float travelR = 1f;

    private static bool groundedL0;
    private static bool groundedR0;

    private static bool groundedL1;
    private static bool groundedR1;

    private static float antiRollForce;


    private static float wheelBase=2.81f;
    private static float rearTrack=1.506f;
    private static float turnRadius=5.5f;


    private static float ackermannAngleleft;
    private static float ackermannAngleRight;
    #endregion

    #region Public Methods

    public static void AntiRollBars(ref Rigidbody rbody,WheelCollider wheelFrontLeft, WheelCollider wheelFrontRight, WheelCollider wheelRearLeft, WheelCollider wheelRearRight)
    {
        groundedL0 = wheelFrontLeft.GetGroundHit(out hit);
        groundedR0 = wheelFrontRight.GetGroundHit(out hit);
        if (groundedL0) travelL = (-wheelFrontLeft.transform.InverseTransformPoint(hit.point).y - wheelFrontLeft.radius) / wheelFrontLeft.suspensionDistance;
        if (groundedR0) travelR = (-wheelFrontRight.transform.InverseTransformPoint(hit.point).y - wheelFrontRight.radius) / wheelFrontRight.suspensionDistance;
        antiRollForce = (travelL - travelR) * AntiRoll;

        if (groundedL0) rbody.AddForceAtPosition(wheelFrontLeft.transform.up * -antiRollForce, wheelFrontLeft.transform.position);
        if (groundedR0) rbody.AddForceAtPosition(wheelFrontRight.transform.up * antiRollForce, wheelFrontRight.transform.position);

        groundedL1 = wheelRearLeft.GetGroundHit(out hit);
        groundedR1 = wheelRearRight.GetGroundHit(out hit);
        if (groundedL1) travelL = (-wheelRearLeft.transform.InverseTransformPoint(hit.point).y - wheelRearLeft.radius) / wheelRearLeft.suspensionDistance;
        if (groundedR1) travelR = (-wheelRearRight.transform.InverseTransformPoint(hit.point).y - wheelRearRight.radius) / wheelRearRight.suspensionDistance;
        antiRollForce = (travelL - travelR) * AntiRoll;

        if (groundedL1) rbody.AddForceAtPosition(wheelRearLeft.transform.up * -antiRollForce, wheelRearLeft.transform.position);
        if (groundedR1) rbody.AddForceAtPosition(wheelRearRight.transform.up * antiRollForce, wheelRearRight.transform.position);

    }
    public static void ApplySteer(ref WheelCollider wheelFrontLeft, ref WheelCollider wheelFrontRight,GameObject Reference,GameObject car, GameObject TargetNode,List<Vector3> splinePoints,int currentIndex, bool Auto,bool Turning,bool usingSteeringWheel)
    {
        float newSteer;
        float newSteerLeft;
        float newSteerRight;
        if (Auto)
        {
            Vector3 relativeVector;
            if (Turning)
            {
                relativeVector = Reference.transform.InverseTransformPoint(splinePoints[currentIndex].x, car.transform.position.y, splinePoints[currentIndex].z);
                newSteer = (relativeVector.x / relativeVector.magnitude) * maxSteerAngle;

                newSteerLeft = newSteer;
                newSteerRight = newSteer;
            }
            else
            {

                relativeVector = Reference.transform.InverseTransformPoint(TargetNode.transform.position.x, car.transform.position.y, TargetNode.transform.position.z);
                newSteer = (relativeVector.x / relativeVector.magnitude) * maxSteerAngle;


                newSteerLeft = newSteer;
                newSteerRight = newSteer;
            }
        }
        else if (usingSteeringWheel)
        {
            LogitechGSDK.DIJOYSTATE2ENGINES rec = LogitechGSDK.LogiGetStateUnity(0);
            newSteer = rec.lX;
            newSteer = ((32767f + newSteer) / 65534f);

            if (newSteer >= 0.5f)
            {
                float newVal = ((newSteer-0.5f) / 0.5f);

                ackermannAngleleft = Mathf.Rad2Deg * Mathf.Atan(wheelBase / (turnRadius + (rearTrack / 2))) * newVal;
                ackermannAngleRight = Mathf.Rad2Deg * Mathf.Atan(wheelBase / (turnRadius - (rearTrack / 2))) * newVal;

            }
            else if (newSteer < 0.5f)
            {
                float newVal = -((0.5f-newSteer) / 0.5f);

                Debug.Log(newVal);

                ackermannAngleleft = Mathf.Rad2Deg * Mathf.Atan(wheelBase / (turnRadius - (rearTrack / 2))) * newVal;
                ackermannAngleRight = Mathf.Rad2Deg * Mathf.Atan(wheelBase / (turnRadius + (rearTrack / 2))) * newVal;
            }

            newSteerLeft = ackermannAngleleft;
            newSteerRight = ackermannAngleRight;
        }
        else
        {

            newSteer = Input.GetAxis("Horizontal") /* maxSteerAngle*/;

            if (newSteer > 0)
            {
                ackermannAngleleft = Mathf.Rad2Deg * Mathf.Atan(wheelBase / (turnRadius + (rearTrack / 2))) * newSteer;
                ackermannAngleRight = Mathf.Rad2Deg * Mathf.Atan(wheelBase / (turnRadius - (rearTrack / 2))) * newSteer;

            }
            else if (newSteer < 0)
            {
                ackermannAngleleft = Mathf.Rad2Deg * Mathf.Atan(wheelBase / (turnRadius - (rearTrack / 2))) * newSteer;
                ackermannAngleRight = Mathf.Rad2Deg * Mathf.Atan(wheelBase / (turnRadius + (rearTrack / 2))) * newSteer;
            }

            newSteerLeft = ackermannAngleleft;
            newSteerRight = ackermannAngleRight;
        }
        wheelFrontLeft.steerAngle = newSteerLeft;
        wheelFrontRight.steerAngle = newSteerRight;
    }
    #endregion
}
