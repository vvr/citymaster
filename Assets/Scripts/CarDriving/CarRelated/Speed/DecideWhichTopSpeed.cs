﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class DecideWhichTopSpeed
{
    public static void DecideWhichSpeedToPick(ref float maxSpeed, float straightTopSpeed, float turnTopSpeed, bool IsCarTurning, bool IsCarPseudoTurning) 
    {
        if (IsCarTurning)
        {
            if (IsCarPseudoTurning) maxSpeed = straightTopSpeed;
            else maxSpeed = turnTopSpeed;
        }
        else maxSpeed = turnTopSpeed;
    }
}
