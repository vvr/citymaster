﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Manual
{
    public static void WASD(ref bool Go, ref bool Stop, ref bool justRoll,ref bool Reverse,ref bool Turning)
    {
        Go = Input.GetKey("w");
        Stop = Input.GetKey("space");
        Reverse = Input.GetKey("s");
        justRoll = !(Go && Stop);
        Turning = Input.GetKey("a") || Input.GetKey("d");
    }

}
