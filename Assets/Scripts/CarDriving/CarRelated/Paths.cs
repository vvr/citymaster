﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Paths 
{
    public string name;
    public string tag;
    public Vector3 position;
    public GameObject gameOJ;

    public Paths(GameObject newGameOJ,string newName, string newTag, Vector3 newPos)
    {
        gameOJ = newGameOJ;
        name = newName;
        tag = newTag;
        position = newPos;
    }
}
