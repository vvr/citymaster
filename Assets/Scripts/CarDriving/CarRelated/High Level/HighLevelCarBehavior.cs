﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class HighLevelCarBehavior
{
    #region Public Methods
    public static void InitializeMethodsForAuto(ref GameObject TargetNode, GameObject car, string currentLane, AgentSimulationEnum agentStatus)
    {
        NodeConfigurationForEachCar.InitialConfiguration(ref TargetNode, car, currentLane, agentStatus);
    }
    public static void CarMechanicalSystem(ref float motorTorque,ref float breakTorque, ref Rigidbody rbody,WheelCollider wheelFrontLeft, WheelCollider wheelFrontRight, WheelCollider wheelRearLeft, WheelCollider wheelRearRight, GameObject Reference,GameObject car,GameObject TargeNode,int currentIndex,List<Vector3> splinePoints,bool GO,bool STOP,bool Auto, bool Reverse,bool justRoll,bool Turning,bool usingSteeringWheel,int gasPedal,int breakPedal)
    {
        ApplyTorques.Drive(ref motorTorque, ref breakTorque, GO, STOP, Auto, Reverse, justRoll, usingSteeringWheel, gasPedal, breakPedal);
        //Steering.AntiRollBars(ref rbody, wheelFrontLeft,wheelFrontRight,wheelRearLeft,wheelRearRight);
        Steering.ApplySteer(ref wheelFrontLeft,ref wheelFrontRight, Reference,car,TargeNode,splinePoints,currentIndex,Auto,Turning,usingSteeringWheel);
    }
    public static void Scanners(ref GameObject frontSideCar,ref float DistanceFromFrontSideCar, ref GameObject rightSideCar, ref float DistanceFromRightSideCar, ref GameObject leftSideCar, ref float DistanceFromLeftSideCar, ref GameObject frontSideObject, ref float DistanceFromFrontSideObject, ref GameObject rightSideObject, ref float DistanceFromRightSideObject, ref GameObject leftSideObject, ref float DistanceFromLeftSideObject,ref bool redLight,ref bool stopSign, GameObject car, string sideLane,Transform Reference,bool turning)
    {
        SensorsForOtherCars.UpdateCarsAndDistances(ref frontSideCar, ref DistanceFromFrontSideCar,
            ref rightSideCar, ref DistanceFromRightSideCar,
            ref leftSideCar, ref DistanceFromLeftSideCar, Reference, sideLane);
    }
    public static void CarSoftwareSystem(ref string sideLane, ref bool GO, ref bool STOP, ref bool Turning, ref int currentIndex, ref bool SlowDown, ref GameObject TargetNode, ref AgentSimulationEnum agentStatus, ref bool canChangeLane, GameObject car, ref float currentSpeed, ref bool carInFrontSTOP, GameObject carInFrontSide, GameObject Reference, ref List<Vector3> splinePoints, string currentLane, ref bool exceedingSpeed,ref bool sideCarNeedsToTurn, ref bool pseudoTurning,float maxSpeed,float maxStraightSpeed, float maxTurnSpeed,bool carInFrontOfMe,GameObject leftSideCar,GameObject rightSideCar,bool redLight,bool stopSign)
    {
        CheckDistancesFromCars.KeepDistanceFromFrontCar(ref carInFrontSTOP, carInFrontSide);
        CarUpdateHelper.UpdateVehicleSpeed(ref currentSpeed,car);
        DecideWhichTopSpeed.DecideWhichSpeedToPick(ref maxSpeed, maxStraightSpeed, maxTurnSpeed, Turning, pseudoTurning);
        ExceedingManager.ExceedingMaxSpeed(ref exceedingSpeed, currentSpeed, maxSpeed);
        SideCarHandler.IsSideCarTurningTowardsMe(ref sideCarNeedsToTurn, leftSideCar, rightSideCar, sideLane);
        SideCarHandler.DecideLanePriority(ref sideCarNeedsToTurn, Turning, currentLane);
        NodeConfigurationForEachCar.QuestionNewNode(ref TargetNode, ref agentStatus,ref canChangeLane,ref Turning,ref splinePoints, car,ref pseudoTurning);
        TurningBehaviour.CheckPoints(ref Turning,ref currentIndex,ref SlowDown, Reference, splinePoints);
        AutoDrive.DrivingDecision(ref STOP, ref GO, exceedingSpeed, carInFrontOfMe, sideCarNeedsToTurn,redLight,stopSign);
        CarUpdateHelper.WhichIsMySideLane(ref sideLane, currentLane);
    }
    #endregion
}
