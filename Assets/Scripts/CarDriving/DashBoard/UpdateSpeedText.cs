﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateSpeedText : MonoBehaviour
{
    void Update()
    {
        int currentSpeed = (int) transform.root.gameObject.GetComponent<Data>().currentSpeed;
        transform.GetComponent<TextMesh>().text = currentSpeed.ToString();
    }
}
