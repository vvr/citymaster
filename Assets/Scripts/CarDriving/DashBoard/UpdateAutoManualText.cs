﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateAutoManualText : MonoBehaviour
{
    void Start()
    {
        if (transform.root.GetComponent<Data>().Auto) transform.GetComponent<TextMesh>().text = "Auto";
        else transform.GetComponent<TextMesh>().text = "Manual";
    }
}
