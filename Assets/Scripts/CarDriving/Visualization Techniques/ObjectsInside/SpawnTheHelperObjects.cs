﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnTheHelperObjects : MonoBehaviour
{
    public UseVisualizationTechnique useVisualizationTechniqueScript;
    void Awake()
    {
        UseVisualizationTechnique.VisualizationTechnique chosenTechnique = useVisualizationTechniqueScript._technique;
        bool enableRoot = true;
        bool enableEverything = true;
        bool rotate = false;

        enableRoot = !(chosenTechnique == UseVisualizationTechnique.VisualizationTechnique.None || 
            chosenTechnique== UseVisualizationTechnique.VisualizationTechnique.MeshesAsIs || 
            chosenTechnique==UseVisualizationTechnique.VisualizationTechnique.MeshesSpheres);
        enableEverything = (chosenTechnique == UseVisualizationTechnique.VisualizationTechnique.Arrow3D || chosenTechnique == UseVisualizationTechnique.VisualizationTechnique.Wedge3D);

        transform.GetComponent<MeshRenderer>().enabled = enableRoot;
        transform.GetChild(0).transform.GetComponent<MeshRenderer>().enabled = enableEverything && enableRoot;

        rotate = (chosenTechnique == UseVisualizationTechnique.VisualizationTechnique.Radar || chosenTechnique == UseVisualizationTechnique.VisualizationTechnique.Minimap);
        if (rotate)
        {
            transform.localRotation = Quaternion.Euler(-90f, 0f, 12.6f);
            transform.localPosition = new Vector3(transform.localPosition.x, 0.5069f, transform.localPosition.z);
        }
    }
}
