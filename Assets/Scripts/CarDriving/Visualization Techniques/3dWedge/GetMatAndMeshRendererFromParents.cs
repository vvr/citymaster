﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetMatAndMeshRendererFromParents : MonoBehaviour
{
    void Start()
    {
        Color parentcolor = transform.parent.GetComponent<MeshRenderer>().material.color;
        transform.GetComponent<MeshRenderer>().material.color = new Color(parentcolor.r,parentcolor.g,parentcolor.b, transform.GetComponent<MeshRenderer>().material.color.a);
    }
    private void Update()
    {
        transform.GetComponent<MeshRenderer>().enabled = transform.parent.GetComponent<MeshRenderer>().enabled;
    }
}
