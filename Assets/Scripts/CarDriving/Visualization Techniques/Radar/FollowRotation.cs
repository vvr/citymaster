﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowRotation : MonoBehaviour 
{
    public GameObject _car;
    public GameObject _hmd;
    public GameObject Heading;
    
    void Update()
    {
        
        Vector3 orientation = _car.GetComponent<Data>().CurrentOrientation;
        Vector3 pos = _car.transform.TransformPoint(new Vector3(0.007877031f, 0.5114f, 0.7649f));
        transform.position = pos;
        //transform.rotation = _hmd.transform.rotation;
        //Vector3 Angle = _hmd.transform.rotation.eulerAngles;   

        Heading.transform.position = transform.position;
        Heading.transform.rotation = transform.rotation;
        if (_car.GetComponent<Data>().currentSpeed>5f)
        {
            if (Mathf.Abs(orientation.z) > 0.8f)
            {
                float signedAngle = Vector3.SignedAngle(_hmd.transform.forward, _car.transform.forward, transform.up);
                float angle = Vector3.Angle(_hmd.transform.forward, _car.transform.forward);
                if (angle > 10f)
                    Heading.transform.eulerAngles = new Vector3(Heading.transform.eulerAngles.x, Heading.transform.eulerAngles.y, signedAngle);
                else
                    Heading.transform.eulerAngles = new Vector3(Heading.transform.eulerAngles.x, Heading.transform.eulerAngles.y, 0f);

            }
            else if (Mathf.Abs(orientation.x) > 0.8f)
            {
                float signedAngle = Vector3.SignedAngle(_hmd.transform.right, _car.transform.right, transform.up);
                float angle = Vector3.Angle(_hmd.transform.right, _car.transform.right);
                if (angle > 10f)
                    Heading.transform.eulerAngles = new Vector3(Heading.transform.eulerAngles.x, Heading.transform.eulerAngles.y, signedAngle);
                else
                    Heading.transform.eulerAngles = new Vector3(Heading.transform.eulerAngles.x, Heading.transform.eulerAngles.y, 0f);
            }
        }
    }
}
