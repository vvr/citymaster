﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cubeFollowsCar : MonoBehaviour
{
    public Transform refTransform;
    public GameObject _objectToFollow;
    public float _threshHold;
    private float _normalize = 0.75f;
    void Update()
    {
        try
        {
            Vector3 newVector = refTransform.InverseTransformPoint(_objectToFollow.transform.position);
            transform.localPosition = new Vector3(((newVector.x) / _threshHold)* _normalize, ((newVector.z) / _threshHold)* _normalize, transform.localPosition.z);
        }
        catch {; }
    }
}
