﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class spawnBoxesForObjects : MonoBehaviour
{
    public GameObject _car;
    public GameObject _objectDisplay;
    public Transform _referenceToSpawnSphereFrom;
    public float _thresholdtoRender;
    private int _carLayer;
    private List<GameObject> carsBeingMonitored = new List<GameObject>();
    private List<GameObject> cubesForCars = new List<GameObject>();

    public GameObject mainCamera;
    private int _buildingsLayer;
    private int _pedestrianLayer;
    private int _scooterLayer;
    private void Awake()
    {
        _carLayer = LayerMask.NameToLayer("Car");
        _buildingsLayer = LayerMask.NameToLayer("Buildings");
        _pedestrianLayer = LayerMask.NameToLayer("Pedestrian");
        _scooterLayer = LayerMask.NameToLayer("Scooter");
    }
    private void Update()
    {
        EnableCubes(_objectDisplay, _car.transform, _referenceToSpawnSphereFrom);
        CheckEveryCarInsideList(ref carsBeingMonitored, _referenceToSpawnSphereFrom);
    }
    private void EnableCubes(GameObject carCubes, Transform car, Transform referenceDriver)
    {
        int layerMaskCar = 1 << _carLayer;
        int layerMaskBuildings = 1 << _buildingsLayer;
        int layerPedestrian = 1 << _pedestrianLayer;
        int layerScooter = 1 << _scooterLayer;
        int layerMask = layerMaskCar | layerPedestrian | layerScooter;
        int layerMaskCarOrBuildings = layerMaskCar | layerMaskBuildings | layerPedestrian | layerScooter;
        Collider[] nearbyCars = Physics.OverlapSphere(referenceDriver.position, _thresholdtoRender, layerMask);
        foreach (Collider carCollid in nearbyCars)
        {
            GameObject carCollider = carCollid.transform.root.gameObject;
            if (!carsBeingMonitored.Contains(carCollider.gameObject) && carCollider.gameObject != car.gameObject)
            {
                if (Physics.Raycast(mainCamera.transform.position, carCollider.gameObject.transform.position - mainCamera.transform.position, out RaycastHit hit, Mathf.Infinity, layerMaskCarOrBuildings))
                {
                    if (hit.collider.gameObject.layer == _buildingsLayer)
                    {
                        carsBeingMonitored.Add(carCollider.gameObject);
                        GameObject cube = Instantiate(carCubes, transform);
                        cube.GetComponent<cubeFollowsCar>()._objectToFollow = carCollider.gameObject;
                        cube.GetComponent<cubeFollowsCar>()._threshHold = _thresholdtoRender;
                        cube.GetComponent<cubeFollowsCar>().refTransform = referenceDriver;
                        cubesForCars.Add(cube);
                    }
                }
            }
        }
    }

    private void CheckEveryCarInsideList(ref List<GameObject> carsBeingMonitored, Transform referenceDriver)
    {
        {
            int layerMaskPlayer = 1 << _carLayer;
            int layerMaskBuildings = 1 << _buildingsLayer;
            int layerPedestrian = 1 << _pedestrianLayer;
            int layerScooter = 1 << _scooterLayer;
            int layerMaskCarOrBuildings = layerMaskPlayer | layerMaskBuildings | layerPedestrian | layerScooter;
            int i = 0;
            foreach (GameObject car in carsBeingMonitored.ToList())
            {
                bool removeIt = true;
                if (Vector3.Distance(referenceDriver.position, car.transform.position) <= _thresholdtoRender)
                {
                    if (Physics.Raycast(mainCamera.transform.position, car.transform.position - mainCamera.transform.position, out RaycastHit hit, layerMaskCarOrBuildings))
                    {
                        removeIt = false;
                        if (hit.collider.gameObject.layer == _buildingsLayer)
                        {
                            cubesForCars[i].GetComponent<MeshRenderer>().enabled = true;
                            cubesForCars[i].GetComponent<MeshRenderer>().enabled = true;
                        }
                        else
                        {
                            cubesForCars[i].GetComponent<MeshRenderer>().enabled = false;
                            cubesForCars[i].GetComponent<MeshRenderer>().enabled = false;
                        }
                    }
                }
                if (removeIt)
                {
                    GameObject Arrow = cubesForCars[i];
                    cubesForCars.RemoveAt(i);
                    Destroy(Arrow);
                    carsBeingMonitored.Remove(car);
                    i--;
                }
                i++;
            }
        }
    }
}
