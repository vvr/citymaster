﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class HelperFuncForMeshes
{
    private static int _carLayer=LayerMask.NameToLayer("Car");
    private static int _buildingsLayer = LayerMask.NameToLayer("Buildings");
    private static int _pedestrianLayer = LayerMask.NameToLayer("Pedestrian");
    private static int _scooterLayer = LayerMask.NameToLayer("Scooter");
    public static void CarsAroundMe (ref List<GameObject> carsBeingMonitored, Transform car, Transform referenceDriver,float thresholdtoRender,GameObject mainCamera,Shader standardShader,Shader overlayShader,bool regularMeshes)
    {
        int layerMaskCar = 1 << _carLayer;
        int layerMaskBuildings = 1 << _buildingsLayer;
        int layerPedestrian = 1 << _pedestrianLayer;
        int layerScooter = 1 << _scooterLayer;
        int layerMask = layerMaskCar | layerPedestrian | layerScooter;
        int layerMaskCarOrBuildings = layerMaskCar | layerMaskBuildings | layerPedestrian | layerScooter;
        Collider[] nearbyCars = Physics.OverlapSphere(referenceDriver.position, thresholdtoRender, layerMask);
        foreach (Collider carCollid in nearbyCars)
        {
            GameObject carCollider = carCollid.transform.root.gameObject;
            if (carCollider.gameObject != car.gameObject)
            {
                if (!carsBeingMonitored.Contains(carCollider.gameObject))
                {
                    if (Physics.Raycast(mainCamera.transform.position, carCollider.gameObject.transform.position - mainCamera.transform.position, out RaycastHit hit, Mathf.Infinity, layerMaskCarOrBuildings))
                    {
                        if (hit.collider.gameObject.layer == _buildingsLayer)
                        {
                            carsBeingMonitored.Add(carCollider.gameObject);
                            if (!regularMeshes)
                            {
                                foreach (Transform child in carCollider.gameObject.transform.GetChild(1).transform)
                                {
                                    try { child.gameObject.GetComponent<MeshRenderer>().enabled = false; }
                                    catch { child.gameObject.GetComponent<SkinnedMeshRenderer>().enabled = false; }
                                }
                                try
                                {
                                    carCollider.gameObject.transform.GetChild(1).transform.GetChild(0).gameObject.GetComponent<MeshRenderer>().enabled = true;
                                    carCollider.gameObject.transform.GetChild(1).transform.GetChild(0).transform.GetComponent<MeshRenderer>().material.shader = overlayShader;
                                    carCollider.gameObject.transform.GetChild(1).transform.GetChild(0).transform.GetComponent<MeshRenderer>().material.SetColor("_Silhouette", new Color(0f, 1f, 0.8799701f, 1f));

                                }
                                catch
                                {
                                    carCollider.gameObject.transform.GetChild(1).transform.GetChild(0).gameObject.GetComponent<SkinnedMeshRenderer>().enabled = true;
                                    carCollider.gameObject.transform.GetChild(1).transform.GetChild(0).transform.GetComponent<SkinnedMeshRenderer>().material.shader = overlayShader;
                                    if (carCollider.gameObject.transform.GetChild(1).transform.GetChild(0).gameObject.layer== _scooterLayer)
                                        carCollider.gameObject.transform.GetChild(1).transform.GetChild(0).transform.GetComponent<SkinnedMeshRenderer>().material.SetColor("_Silhouette", new Color(1f, 1f, 0f, 1f));
                                    else
                                        carCollider.gameObject.transform.GetChild(1).transform.GetChild(0).transform.GetComponent<SkinnedMeshRenderer>().material.SetColor("_Silhouette", new Color(1f, 0f, 0f, 1f));
                                }
                            }
                            else
                            {
                                try
                                {
                                    carCollider.gameObject.transform.GetChild(1).transform.GetChild(1).transform.GetComponent<MeshRenderer>().material.shader = overlayShader;
                                }
                                catch 
                                { 
                                    carCollider.gameObject.transform.GetChild(1).transform.GetChild(1).transform.GetComponent<SkinnedMeshRenderer>().material.shader = overlayShader;
                                }
                                //carCollider.GetComponent<Collider>().gameObject.GetComponentInChildren<AnnotationObjects>().displayAnnotations = true;
                            }
                        }
                        else
                        {
                            try
                            {
                                carCollider.gameObject.transform.GetChild(1).transform.GetChild(1).transform.GetComponent<MeshRenderer>().material.shader = standardShader;

                            }
                            catch { carCollider.gameObject.transform.GetChild(1).transform.GetChild(1).transform.GetComponent<SkinnedMeshRenderer>().material.shader = standardShader; }
                            //carCollider.GetComponent<Collider>().gameObject.GetComponentInChildren<AnnotationObjects>().displayAnnotations = true;
                        }
                    }
                }
            }
        }
    }

    public static void CheckEveryCarInsideList(ref List<GameObject> carsBeingMonitored, Transform referenceDriver, float _thresholdtoRender, GameObject mainCamera, Shader standardShader, Shader overlayShader, bool regularMeshes)
    {
        int layerMaskPlayer = 1 << _carLayer;
        int layerMaskBuildings = 1 << _buildingsLayer;
        int layerPedestrian = 1 << _pedestrianLayer;
        int layerScooter = 1 << _scooterLayer;
        int layerMaskCarOrBuildings = layerMaskPlayer | layerMaskBuildings | layerPedestrian | layerScooter;
        int i = 0;
        foreach (GameObject car in carsBeingMonitored.ToList())
        {
            bool removeIt = true;
            if (Vector3.Distance(referenceDriver.position, car.transform.position) <= _thresholdtoRender)
            {
                if (Physics.Raycast(mainCamera.transform.position, car.transform.position - mainCamera.transform.position, out RaycastHit hit, layerMaskCarOrBuildings))
                {
                    removeIt = false;
                    if (hit.collider.gameObject.layer == _buildingsLayer)
                    {
                        if (!regularMeshes)
                        {
                            foreach (Transform child in car.gameObject.transform.GetChild(1).gameObject.transform)
                            {
                                try
                                {
                                    child.gameObject.GetComponent<MeshRenderer>().enabled = false;
                                }
                                catch { child.gameObject.GetComponent<SkinnedMeshRenderer>().enabled = false; }
                            }
                            try
                            {
                                car.gameObject.transform.GetChild(1).transform.GetChild(0).gameObject.GetComponent<MeshRenderer>().enabled = true;
                                car.gameObject.transform.GetChild(1).transform.GetChild(1).transform.GetComponent<MeshRenderer>().material.shader = overlayShader;
                            }
                            catch
                            {
                                car.gameObject.transform.GetChild(1).transform.GetChild(0).gameObject.GetComponent<SkinnedMeshRenderer>().enabled = true;
                                car.gameObject.transform.GetChild(1).transform.GetChild(1).transform.GetComponent<SkinnedMeshRenderer>().material.shader = overlayShader;
                            }
                        }
                        else
                        {
                            foreach (Transform child in car.gameObject.transform.GetChild(1).gameObject.transform)
                            {
                                try { child.gameObject.GetComponent<MeshRenderer>().enabled = true; }
                                catch { child.gameObject.GetComponent<SkinnedMeshRenderer>().enabled = true; }
                            }
                            try
                            {
                                car.gameObject.transform.GetChild(1).transform.GetChild(0).gameObject.GetComponent<MeshRenderer>().enabled = false;
                                car.gameObject.transform.GetChild(1).transform.GetChild(1).transform.GetComponent<MeshRenderer>().material.shader = overlayShader;
                            }
                            catch
                            {
                                car.gameObject.transform.GetChild(1).transform.GetChild(0).gameObject.GetComponent<SkinnedMeshRenderer>().enabled = false;
                                car.gameObject.transform.GetChild(1).transform.GetChild(1).transform.GetComponent<SkinnedMeshRenderer>().material.shader = overlayShader;
                            }
                        }
                    }
                    else
                    {
                        foreach (Transform child in car.gameObject.transform.GetChild(1).transform)
                        {
                            try { child.gameObject.GetComponent<MeshRenderer>().enabled = true; }
                            catch { child.gameObject.GetComponent<SkinnedMeshRenderer>().enabled = true; }
                        }
                        try
                        {
                            car.gameObject.transform.GetChild(1).transform.GetChild(0).gameObject.GetComponent<MeshRenderer>().enabled = false;
                            car.gameObject.transform.GetChild(1).transform.GetChild(1).transform.GetComponent<MeshRenderer>().material.shader = standardShader;
                        }
                        catch
                        {
                            car.gameObject.transform.GetChild(1).transform.GetChild(0).gameObject.GetComponent<SkinnedMeshRenderer>().enabled = false;
                            car.gameObject.transform.GetChild(1).transform.GetChild(1).transform.GetComponent<SkinnedMeshRenderer>().material.shader = standardShader;
                        }
                    }
                }
            }
            if (removeIt)
            {
                foreach (Transform child in car.gameObject.transform.GetChild(1).transform)
                {
                    try { child.gameObject.GetComponent<MeshRenderer>().enabled = true; }
                    catch { child.gameObject.GetComponent<SkinnedMeshRenderer>().enabled = true; }
                }
                try
                {
                    car.gameObject.transform.GetChild(1).transform.GetChild(0).gameObject.GetComponent<MeshRenderer>().enabled = false;
                    car.gameObject.transform.GetChild(1).transform.GetChild(1).transform.GetComponent<MeshRenderer>().material.shader = standardShader;
                }
                catch
                {
                    car.gameObject.transform.GetChild(1).transform.GetChild(0).gameObject.GetComponent<SkinnedMeshRenderer>().enabled = false;
                    car.gameObject.transform.GetChild(1).transform.GetChild(1).transform.GetComponent<SkinnedMeshRenderer>().material.shader = standardShader;
                }
                carsBeingMonitored.Remove(car);
            }
        }
    }
}
