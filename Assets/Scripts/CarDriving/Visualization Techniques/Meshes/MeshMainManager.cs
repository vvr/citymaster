﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshMainManager : MonoBehaviour
{
    public bool regularMeshes;
    private List<GameObject> carsAroundMe = new List<GameObject>();
    public Transform car;
    public Transform _referenceDriver;
    public float _thresholdToRender;
    public GameObject mainCamera;
    private Shader standardShader;
    private Shader overlayShader;
    void Start()
    {
        standardShader = Shader.Find("Standard");
        overlayShader = Shader.Find("Custom/Overlays");
    }
    void Update()
    {
        HelperFuncForMeshes.CarsAroundMe(ref carsAroundMe, car, _referenceDriver, _thresholdToRender, mainCamera, standardShader, overlayShader,regularMeshes);
        //foreach (GameObject car in carsAroundMe)
        //{
        //    //car.gameObject.GetComponentInChildren<AnnotationObjects>().gameObject.transform.rotation = Quaternion.LookRotation(mainCamera.transform.position - car.gameObject.transform.position);
        //    //car.gameObject.GetComponentInChildren<AnnotationObjects>().gameObject.transform.GetChild(0).gameObject.GetComponent<TextMesh>().text = "Speed: " + car.gameObject.GetComponent<Data>().currentSpeed.ToString("0") + " km/h";
        //    //car.gameObject.GetComponentInChildren<AnnotationObjects>().gameObject.transform.GetChild(1).gameObject.GetComponent<TextMesh>().text = "Distance: " + Vector3.Distance(mainCamera.transform.position, car.transform.position).ToString("0");
        //}
        HelperFuncForMeshes.CheckEveryCarInsideList(ref carsAroundMe, _referenceDriver, _thresholdToRender, mainCamera, standardShader, overlayShader, regularMeshes);
    }
}
