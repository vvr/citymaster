﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UseVisualizationTechnique : MonoBehaviour
{
    public enum VisualizationTechnique { None,Arrow3D,Minimap,Radar,Wedge3D,MeshesAsIs,MeshesSpheres};
    public VisualizationTechnique _technique;
    public float _thresholdToRender = 50f;

    public GameObject _hmd;
    public GameObject _car;
    public Transform _driverReference;

    public GameObject _referencePoint;
    public GameObject _radar;
    public GameObject _heading;
    public GameObject _minimap;
    public GameObject _fov;

    public GameObject _meshManager;
    public GameObject MainCamera;
    private void Start()
    {
        switch (_technique)
        {
            case VisualizationTechnique.Arrow3D:
            case VisualizationTechnique.Wedge3D:
                ReferencePointOriented(_car,_hmd,_technique);
                break;
            case VisualizationTechnique.Radar:
            case VisualizationTechnique.Minimap:
                RadarsOriented(_car, _hmd, _technique);
                break;
            case VisualizationTechnique.MeshesAsIs:
            case VisualizationTechnique.MeshesSpheres:
                MeshOriented(_car, _hmd, _technique);
                break;
            default:
                break;
        }
    }
    private void ReferencePointOriented(GameObject car, GameObject hmd,VisualizationTechnique chosenTechnique)
    {
        GameObject ReferencePoint=Instantiate(_referencePoint, hmd.transform);
        ReferencePoint.GetComponent<RenderTechnique>()._thresholdtoRender = _thresholdToRender;
        ReferencePoint.GetComponent<RenderTechnique>()._referenceToSpawnSphereFrom = _driverReference;
        ReferencePoint.GetComponent<RenderTechnique>()._car = car;
        ReferencePoint.GetComponent<RenderTechnique>().mainCamera = MainCamera.gameObject;
        ReferencePoint.GetComponent<RenderTechnique>()._selectedTechnique = chosenTechnique;
    }

    private void RadarsOriented (GameObject car,GameObject hmd,VisualizationTechnique chosenTechnique)
    {
        GameObject referenceToBeSpawn=null;
        GameObject headingToBeSpawn = null;
        if (chosenTechnique == VisualizationTechnique.Radar)
        {
            referenceToBeSpawn = _radar;
            headingToBeSpawn = _heading;
        }
        else if (chosenTechnique == VisualizationTechnique.Minimap)
        {
            referenceToBeSpawn = _minimap;
            headingToBeSpawn = _fov;
        }

        GameObject Radar = Instantiate(referenceToBeSpawn, hmd.transform);
        Radar.GetComponent<FollowRotation>()._hmd = MainCamera.gameObject;
        Radar.GetComponent<FollowRotation>()._car = car;

        GameObject heading = Instantiate(headingToBeSpawn);
        Radar.GetComponent<FollowRotation>().Heading = heading;
        Radar.GetComponent<spawnBoxesForObjects>()._thresholdtoRender = _thresholdToRender;
        Radar.GetComponent<spawnBoxesForObjects>()._referenceToSpawnSphereFrom = _driverReference;
        Radar.GetComponent<spawnBoxesForObjects>()._car = car;
        Radar.GetComponent<spawnBoxesForObjects>().mainCamera = MainCamera;

    }
    private void MeshOriented(GameObject car, GameObject hmd, VisualizationTechnique chosenTechnique)
    {
        GameObject manager = Instantiate(_meshManager);
        manager.GetComponent<MeshMainManager>().regularMeshes = chosenTechnique == VisualizationTechnique.MeshesAsIs;
        manager.GetComponent<MeshMainManager>().car = car.transform;
        manager.GetComponent<MeshMainManager>()._referenceDriver = _driverReference;
        manager.GetComponent<MeshMainManager>()._thresholdToRender = _thresholdToRender;
        manager.GetComponent<MeshMainManager>().mainCamera = MainCamera.gameObject;
    }
    [ExecuteInEditMode]
    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireSphere(_car.transform.position, _thresholdToRender);
    }
}
