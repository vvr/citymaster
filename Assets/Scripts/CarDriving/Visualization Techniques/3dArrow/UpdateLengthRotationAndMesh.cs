﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdateLengthRotationAndMesh : MonoBehaviour
{
    public Transform refTransform;
    public GameObject _objectToFollow;
    public float _threshHold;
    private float limitToShow=3f;
    public float _prevDist;

    public MeshRenderer meshRendererStick;
    public MeshRenderer meshRendererPoint;

    void Update()
    {
        try
        {
            Vector3 _dist = refTransform.position - _objectToFollow.transform.position;
            _dist.y = 0;
            float newVal=((_dist.magnitude)/ _threshHold)*limitToShow;//MinMaxNormalization 
            transform.localScale = new Vector3(transform.localScale.x,refTransform.localScale.y, newVal);
            transform.LookAt(_objectToFollow.transform);
            _prevDist = refTransform.InverseTransformPoint(_objectToFollow.transform.position).magnitude;
        }
        catch { ; }
    }

}
