﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickAMat : MonoBehaviour
{
    public GameObject parent;
    void Start()
    {
        LayerMask carLayer = LayerMask.NameToLayer("Car");
        LayerMask  pedestrianLayer = LayerMask.NameToLayer("Pedestrian");
        LayerMask scooterLayer = LayerMask.NameToLayer("Scooter");

        if (parent.GetComponent<UpdateLengthRotationAndMesh>() != null)
        {
            if (parent.GetComponent<UpdateLengthRotationAndMesh>()._objectToFollow.layer == carLayer.value)
            {
                transform.GetComponent<MeshRenderer>().material.color = new Color(0f, 1f, 0.8799701f);
            }
            else if (parent.GetComponent<UpdateLengthRotationAndMesh>()._objectToFollow.layer == pedestrianLayer.value)
            {
                transform.GetComponent<MeshRenderer>().material.color = new Color(1f, 0f, 0f);
            }
            else if(parent.GetComponent<UpdateLengthRotationAndMesh>()._objectToFollow.layer == scooterLayer.value)
            {
                transform.GetComponent<MeshRenderer>().material.color = new Color(1f, 1f, 0f);
            }
        }
        else if (parent.GetComponent<cubeFollowsCar>() != null)
        {
            if (parent.GetComponent<cubeFollowsCar>()._objectToFollow.layer == carLayer.value)
            {
                transform.GetComponent<MeshRenderer>().material.color = new Color(0f, 1f, 0.8799701f);
            }
            else if (parent.GetComponent<cubeFollowsCar>()._objectToFollow.layer == pedestrianLayer.value)
            {
                transform.GetComponent<MeshRenderer>().material.color = new Color(1f, 0f, 0f);
            }
            else if (parent.GetComponent<cubeFollowsCar>()._objectToFollow.layer == scooterLayer.value)
            {
                transform.GetComponent<MeshRenderer>().material.color = new Color(1f, 1f, 0f);
            }
        }
    }
}
