﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.InteropServices;
using UnityEditor;
using UnityEngine;

public class RenderTechnique : MonoBehaviour
{
    public UseVisualizationTechnique.VisualizationTechnique _selectedTechnique;
    public GameObject _car;
    public GameObject _arrow3D;
    public GameObject _3dWedge;
    public Transform _referenceToSpawnSphereFrom;
    public float _thresholdtoRender;
    private int _carLayer;
    private int _buildingsLayer;
    private int _pedestrianLayer;
    private int _scooterLayer;
    private List<GameObject> carsBeingMonitored = new List<GameObject>();
    private List<GameObject> Arrows3D = new List<GameObject>();

    public GameObject mainCamera;
    private void Awake()
    {
        _carLayer = LayerMask.NameToLayer("Car");
        _buildingsLayer = LayerMask.NameToLayer("Buildings");
        _pedestrianLayer = LayerMask.NameToLayer("Pedestrian");
        _scooterLayer = LayerMask.NameToLayer("Scooter");

    }
    private void Update()
    {
        if (_selectedTechnique == UseVisualizationTechnique.VisualizationTechnique.Arrow3D) EnableArrows(_arrow3D, _car.transform, _referenceToSpawnSphereFrom);
        else if(_selectedTechnique == UseVisualizationTechnique.VisualizationTechnique.Wedge3D) EnableArrows(_3dWedge, _car.transform, _referenceToSpawnSphereFrom);
        CheckEveryCarInsideList(ref carsBeingMonitored, _referenceToSpawnSphereFrom);
        GetComponent<MeshRenderer>().enabled = (carsBeingMonitored.Count != 0 && IsAtLeastOneArrowRendered(Arrows3D));
    }

    private bool IsAtLeastOneArrowRendered(List<GameObject> arrows3D)
    {
        for (int i=0; i<arrows3D.Count;i++) { if (arrows3D[i].GetComponent<UpdateLengthRotationAndMesh>().meshRendererStick.enabled==true) return true; }
        return false;
    }

    private void EnableArrows(GameObject arrow3D, Transform car, Transform referenceDriver)
    {
        int layerMaskCar = 1 << _carLayer;
        int layerMaskBuildings = 1 << _buildingsLayer;
        int layerPedestrian = 1 << _pedestrianLayer;
        int layerScooter = 1 << _scooterLayer;
        int layerMask = layerMaskCar | layerPedestrian | layerScooter;
        int layerMaskCarOrBuildings = layerMaskCar | layerMaskBuildings| layerPedestrian | layerScooter;
        Collider[] nearbyCars = Physics.OverlapSphere(referenceDriver.position, _thresholdtoRender, layerMask);
        foreach (Collider carCollid in nearbyCars)
        {
            GameObject carCollider = carCollid.transform.root.gameObject;
            if (carCollider.gameObject != car.gameObject)
            {
                if (Mathf.Abs(Vector3.Angle(_referenceToSpawnSphereFrom.position - mainCamera.transform.position,carCollider.gameObject.transform.position - mainCamera.transform.position))<90f)
                {
                    if (!carsBeingMonitored.Contains(carCollider.gameObject))
                    {
                        if (Physics.Raycast(mainCamera.transform.position, carCollider.gameObject.transform.position - mainCamera.transform.position, out RaycastHit hit, Mathf.Infinity, layerMaskCarOrBuildings))
                        {
                            if (hit.collider.gameObject.layer == _buildingsLayer)
                            {
                                carsBeingMonitored.Add(carCollider.gameObject);
                                GameObject arrow = Instantiate(arrow3D, transform);
                                arrow.GetComponent<UpdateLengthRotationAndMesh>()._objectToFollow = carCollider.gameObject;
                                arrow.GetComponent<UpdateLengthRotationAndMesh>().refTransform = referenceDriver;
                                arrow.GetComponent<UpdateLengthRotationAndMesh>()._threshHold = _thresholdtoRender;

                                Arrows3D.Add(arrow);
                            }
                        }
                    }
                }
            }
        }
    }

    private void CheckEveryCarInsideList(ref List<GameObject> carsBeingMonitored, Transform referenceDriver)
    {
        int layerMaskPlayer = 1 << _carLayer;
        int layerMaskBuildings = 1 << _buildingsLayer;
        int layerPedestrian = 1 << _pedestrianLayer;
        int layerScooter = 1 << _scooterLayer;
        int layerMaskCarOrBuildings = layerMaskPlayer | layerMaskBuildings | layerPedestrian | layerScooter;
        int i = 0;
        foreach (GameObject car in carsBeingMonitored.ToList())
        {
            bool removeIt = true;
            if (Vector3.Distance(referenceDriver.position, car.transform.position) <= _thresholdtoRender)
            {
                if (Mathf.Abs(Vector3.Angle(_referenceToSpawnSphereFrom.position - mainCamera.transform.position, car.transform.position - mainCamera.transform.position)) < 90f)
                {
                    if (Physics.Raycast(mainCamera.transform.position, car.transform.position - mainCamera.transform.position, out RaycastHit hit, layerMaskCarOrBuildings))
                    {
                        removeIt = false;
                        if (hit.collider.gameObject.layer == _buildingsLayer)
                        {
                            Arrows3D[i].GetComponent<UpdateLengthRotationAndMesh>().meshRendererPoint.enabled = true;
                            Arrows3D[i].GetComponent<UpdateLengthRotationAndMesh>().meshRendererStick.enabled = true;
                        }
                        else
                        {
                            Arrows3D[i].GetComponent<UpdateLengthRotationAndMesh>().meshRendererPoint.enabled = false;
                            Arrows3D[i].GetComponent<UpdateLengthRotationAndMesh>().meshRendererStick.enabled = false;
                        }
                    }
                }
            }
            if (removeIt)
            {
                GameObject Arrow=Arrows3D[i];
                Arrows3D.RemoveAt(i);
                Destroy(Arrow);
                carsBeingMonitored.Remove(car);
                i--;
            }
            i++;
        }
    }
}
