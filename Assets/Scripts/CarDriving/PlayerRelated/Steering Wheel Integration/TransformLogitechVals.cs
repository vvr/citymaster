﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransformLogitechVals : MonoBehaviour
{
    public GameObject playerCar;
    private LogitechGSDK.DIJOYSTATE2ENGINES rec;
    void Update()
    {
        if (!playerCar.GetComponent<Data>().Auto)
        {
            rec = LogitechGSDK.LogiGetStateUnity(0);
            if (rec.lX == 0 && rec.lY == 0 && rec.lRz == 0) TransformSteeringWheelValues.FixInitialVals(ref rec.lY, ref rec.lRz);
            TransformSteeringWheelValues.RegularTransform(ref rec.lY, ref rec.lRz);
            playerCar.GetComponent<Data>().gasPedal = rec.lY;
            playerCar.GetComponent<Data>().breakPedal = rec.lRz;
        }
    }
}
