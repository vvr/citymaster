﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class TransformSteeringWheelValues
{
    private static float maxTorque = 250f;
    private static float maxBreakTorque = 1000f;
    public static void FixInitialVals(ref int gasPedal, ref int breakPedal)
    {
        gasPedal = 32767;
        breakPedal = 32767;
    }

    public static void RegularTransform(ref int gasPedal, ref int breakPedal)
    {
        gasPedal = gasPedal * (-1) + 32767;
        float temp0 = (gasPedal / 65535f)* maxTorque;
        gasPedal = (int)temp0;


        breakPedal = breakPedal * (-1) + 32767;
        float temp1 = (breakPedal / 65535f) * maxBreakTorque;
        breakPedal = (int)temp1;
    }
}
