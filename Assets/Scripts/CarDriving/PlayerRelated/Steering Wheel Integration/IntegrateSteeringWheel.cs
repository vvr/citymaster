﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntegrateSteeringWheel : MonoBehaviour
{
    public GameObject steeringWheelGameObject;
    void Awake()
    {
        transform.GetComponent<Data>().usingSteeringWheel = steeringWheelGameObject.activeSelf;
    }
}
