﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoManualSwitcher : MonoBehaviour
{
    private Data dataScript;
    public enum Mode {StartedAuto,StartedManual,Manual,Auto};
    private Mode carMode;
    private void Awake()
    {
        dataScript = transform.GetComponent<Data>();
        if (!dataScript.Auto)
        {
            carMode = Mode.Manual;
            FindYourTargetNode(ref dataScript.targetNode, ref dataScript.CurrentLane, ref dataScript.agentStatus, dataScript.gameObject);
        }
        else
        {
            FindYourTargetNode(ref dataScript.targetNode, ref dataScript.CurrentLane, ref dataScript.agentStatus, dataScript.gameObject);
            carMode = Mode.Auto;
        }

    }
    private void Update()
    {
        switch (carMode)
        {
            case Mode.Manual:
                if (dataScript.Auto) 
                {
                    carMode = Mode.Auto;
                    dataScript.Turning = false;
                    FindYourTargetNode(ref dataScript.targetNode, ref dataScript.CurrentLane, ref dataScript.agentStatus, dataScript.gameObject);
                    
                }
                break;
            case Mode.Auto:
                if (dataScript.Turning) dataScript.Auto = true;
                else if (!dataScript.Auto)
                {
                    carMode = Mode.Manual;
                    dataScript.Turning = false;
                }
                break;
            default:
                break;
        }
    }

    private void FindYourTargetNode(ref GameObject targetNode,ref string tag,ref AgentSimulationEnum agentStatus, GameObject car)
    {
        float _radiusThreshold = 500f;
        LayerMask startPointLayer= LayerMask.NameToLayer("Path_Start"); 
        LayerMask midPointLayer= LayerMask.NameToLayer("Path_MidPoint");
        int _startPointLayerMask = 1 << startPointLayer;
        int _MidPointLayerMask = 1 << midPointLayer;
        int _eitherStartOrMidPointMask = _startPointLayerMask | _MidPointLayerMask;
        float _min = 1000f;
        GameObject target=null;
        Collider[] points = Physics.OverlapSphere(car.transform.position, _radiusThreshold, _eitherStartOrMidPointMask);
        for (int i = 0; i < points.Length; i++)
        {
            float dist = Vector3.Distance(car.transform.position,points[i].transform.position);
            if (dist < _min)
            {
                _min = dist;
                target = points[i].gameObject;
            }
        }
        if (target.layer == startPointLayer)
        {
            //int index = UnityEngine.Random.Range(0, target.GetComponent<StartPoints>()._firstMidPoint.Length);
            targetNode = target.GetComponent<StartPoints>()._firstMidPoint;
        }
        else targetNode = target;
        tag = targetNode.tag;
        agentStatus = AgentSimulationEnum.MovingTowardsMidPoint;
    }
}
