﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnMechanisms : MonoBehaviour
{
    public Data carData;
    public GameObject ClickRegisterMechanism;
    void Start()
    {
        ClickRegisterMechanism.SetActive(carData.Auto);
    }

}
