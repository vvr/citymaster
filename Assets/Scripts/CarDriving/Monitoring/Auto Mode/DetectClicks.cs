﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DetectClicks : MonoBehaviour
{
    public bool click = false;
    private int currentActivatedIndex = 0;
    private int amountOfClicks;
    private GameObject[] flags;
    private float timer = 0f;
    private bool once = false;
    private bool _steeringWheelIsUsed = false;
    void Start()
    {
        _steeringWheelIsUsed = transform.root.gameObject.GetComponent<Data>().usingSteeringWheel;
        amountOfClicks = transform.childCount;
        flags = new GameObject[amountOfClicks];
        int i = 0;
        foreach (Transform child in transform)
        {
            flags[i] = child.gameObject;
            i++;
        }
    }

    // Update is called once per frame
    void Update()
    {
        int preVal = currentActivatedIndex;
        if (timer > 0.5f)
        {
            timer = 0f;
            once = false;
        }
        timer += Time.deltaTime;
        if (_steeringWheelIsUsed)
        {
            LogitechGSDK.DIJOYSTATE2ENGINES rec;
            rec = LogitechGSDK.LogiGetStateUnity(0);
            if ((Input.GetKeyDown("space") || rec.rgbButtons[4] == 128 || rec.rgbButtons[5] == 128) && (timer <= 0.5f && !once))
            {
                once = true;
                if (currentActivatedIndex < amountOfClicks)
                {
                    flags[currentActivatedIndex].GetComponent<MeshRenderer>().enabled = true;
                    currentActivatedIndex++;
                }
            }
        }
        else
        {
            if ((Input.GetKeyDown("space") && (timer <= 0.5f && !once)))
            {
                once = true;
                if (currentActivatedIndex < amountOfClicks)
                {
                    flags[currentActivatedIndex].GetComponent<MeshRenderer>().enabled = true;
                    currentActivatedIndex++;
                }
            }
        }
        if (currentActivatedIndex > preVal) click = true;
        else click = false;
    }
}
