﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public static class Helper
{
    public static GameObject FindClosestGameObject(GameObject[] pathPointsGameObjects, GameObject refTransform, string Layer)
    {
        float minDist = 100000f;
        GameObject nearestGJ = null;
        int i;
        for (i = 0; i < pathPointsGameObjects.Length; i++)
        {
            if (refTransform.transform.InverseTransformPoint(pathPointsGameObjects[i].transform.position).magnitude < minDist && refTransform.CompareTag(pathPointsGameObjects[i].tag) && pathPointsGameObjects[i].layer == LayerMask.NameToLayer(Layer))
            {
                minDist = refTransform.transform.InverseTransformPoint(pathPointsGameObjects[i].transform.position).magnitude;
                nearestGJ = pathPointsGameObjects[i];
            }
        }
        return nearestGJ;
    }

    public static GameObject FindNearestGameObjectFromSphereCollider(GameObject child, string desiredTag, string desiredLayer)
    {
        Collider[] objects = Physics.OverlapSphere(child.transform.position, Mathf.Infinity);
        GameObject nearestGJ = null;
        float minDist = 100000f;
        int i;
        for (i = 0; i < objects.Length; i++)
        {
            if (child.transform.InverseTransformPoint(objects[i].transform.position).magnitude < minDist && objects[i].gameObject.layer == LayerMask.NameToLayer(desiredLayer) && objects[i].gameObject.CompareTag(desiredTag))
            {
                minDist = child.transform.InverseTransformPoint(objects[i].gameObject.transform.position).magnitude;
                nearestGJ = objects[i].gameObject;
            }
        }
        return nearestGJ;
    }

    public static GameObject SelectRandomAvailablePoint(GameObject[] points)
    {
        return points[UnityEngine.Random.Range(0, points.Length)];
    }
}
