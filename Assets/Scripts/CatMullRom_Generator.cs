﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class CatMullRom_Generator : MonoBehaviour
{
	private static int amountOfPoints = 100;
	private static float alpha = 0.5f; //CatMull Rom
	public static List<Vector3> CatmulRom(Transform[] points)
	{
		List<Vector3> newPoints = new List<Vector3>();

		for (int i = 0; i < points.Length - 3; i++)
		{
			Vector3 p0 = points[i].position;
			Vector3 p1 = points[i + 1].position;
			Vector3 p2 = points[i + 2].position;
			Vector3 p3 = points[i + 3].position;

			float t0 = 0.0f;
			float t1 = GetT(t0, p0, p1);
			float t2 = GetT(t1, p1, p2);
			float t3 = GetT(t2, p2, p3);

			for (float t = t1; t < t2; t += ((t2 - t1) / amountOfPoints))
			{
				Vector3 A1 = (t1 - t) / (t1 - t0) * p0 + (t - t0) / (t1 - t0) * p1;
				Vector3 A2 = (t2 - t) / (t2 - t1) * p1 + (t - t1) / (t2 - t1) * p2;
				Vector3 A3 = (t3 - t) / (t3 - t2) * p2 + (t - t2) / (t3 - t2) * p3;

				Vector3 B1 = (t2 - t) / (t2 - t0) * A1 + (t - t0) / (t2 - t0) * A2;
				Vector3 B2 = (t3 - t) / (t3 - t1) * A2 + (t - t1) / (t3 - t1) * A3;

				Vector3 C = (t2 - t) / (t2 - t1) * B1 + (t - t1) / (t2 - t1) * B2;
				newPoints.Add(C);
			}
		}

		return newPoints;
	}

	private static float GetT(float t, Vector3 p0, Vector3 p1)
	{
		float a = Mathf.Pow((p1.x - p0.x), 2.0f) + Mathf.Pow((p1.y - p0.y), 2.0f) + Mathf.Pow((p1.z - p0.z), 2.0f);
		float b = Mathf.Pow(a, 0.5f);
		float c = Mathf.Pow(b, alpha);

		return (c + t);
	}
}