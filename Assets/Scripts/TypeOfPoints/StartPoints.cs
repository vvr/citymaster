﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartPoints : MonoBehaviour
{
    public GameObject _firstMidPoint;

    public static GameObject FindSideStartPoint(GameObject refStartPoint)
    {
        String tag;
        switch (refStartPoint.tag)
        {
            case "Lane1":
                tag = "Lane2";
                break;
            case "Lane2":
                tag = "Lane1";
                break;
            case "Lane3":
                tag = "Lane4";
                break;
            case "Lane4":
                tag = "Lane3";
                break;
            default:
                tag = "";
                break;
        }
        return Helper.FindNearestGameObjectFromSphereCollider(refStartPoint, tag, "Path_Start");
    }
    public static GameObject AssignTargetMidPoint(GameObject startPoint)
    {
        LayerMask midPointLayer = LayerMask.NameToLayer("Path_MidPoint");
        int midPointLayerMask = 1 << midPointLayer;
        if (Physics.Raycast(startPoint.transform.position, startPoint.transform.right, out RaycastHit hit, Mathf.Infinity, midPointLayerMask))
        {
            if (hit.collider.gameObject.CompareTag(startPoint.tag))
            {
                return hit.collider.gameObject;
            }
        }
        return null;
    }

    private static void AddPointsStraightAhead(ref List<GameObject> midPointList, Transform car, Transform refPoint, String tag, int finalMask)
    {
        if (Physics.Raycast(refPoint.transform.position, car.transform.forward, out RaycastHit hit, Mathf.Infinity, finalMask))
        {
            if (hit.collider.gameObject.CompareTag(tag))
            {
                midPointList.Add(hit.collider.gameObject);
                AddPointsStraightAhead(ref midPointList, car, hit.collider.transform, tag, finalMask);
            }
        }
    }
}
