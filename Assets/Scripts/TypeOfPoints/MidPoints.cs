﻿using System.Collections;
using System.Collections.Generic;
using System.Net.Configuration;
using UnityEngine;

public class MidPoints : MonoBehaviour
{
    [Header("Start Points - Splines")]
    public static float _sphereRadius = 20f;
    public GameObject[] _targetStartPoints;
    [System.Serializable]
    public class TurnPoints
    {
        public bool shortTurn = false;
        public Vector3[] points;
    }
    public TurnPoints[] startTargets;
    public GameObject StartPointDirectlyBehindMe;

    public enum Display { None, Splines, SphereReach };
    public Display chooseDisplay = Display.None;

    private static float _pointRad = 0.5f; //To draw the red spheres
    public static GameObject[] StartPointsInRange(Transform midPointRef, GameObject StartPointDirectlyBehind, string tag)
    {
        LayerMask startLayer = LayerMask.GetMask("Path_Start");
        List<GameObject> listToBeReturned = new List<GameObject>();
        if (midPointRef.gameObject.GetComponent<RoundAboutPoints>() != null)
        {
            if (Physics.Raycast(midPointRef.transform.position, midPointRef.transform.right, out RaycastHit hit, Mathf.Infinity, startLayer)) listToBeReturned.Add(hit.collider.gameObject);
        }
        else
        {
            Collider[] gameObjects = Physics.OverlapSphere(midPointRef.position, _sphereRadius, startLayer);
            foreach (Collider collider in gameObjects) if (collider.gameObject.CompareTag(tag) && collider.gameObject != StartPointDirectlyBehind && !IsBehindMe(midPointRef.transform, collider.gameObject.transform.position)) listToBeReturned.Add(collider.gameObject);
        }
        return listToBeReturned.ToArray();
    }

    public static TurnPoints[] GenerateTurns(GameObject midPoint, GameObject[] startPoints)
    {
        List<TurnPoints> turnPoints = new List<TurnPoints>();
        for (int i = 0; i < startPoints.Length; i++)
        {
            TurnPoints newTurnPointArrayOfArrays = new TurnPoints();
            newTurnPointArrayOfArrays.points = CatMullRom_Generator.CatmulRom(ProvideTheControlPoints(midPoint, startPoints[i], ref newTurnPointArrayOfArrays.shortTurn)).ToArray();
            turnPoints.Add(newTurnPointArrayOfArrays);
        }
        return turnPoints.ToArray();
    }

    public static Transform[] ProvideTheControlPoints(GameObject midPoint, GameObject startPoint, ref bool shortTurn)
    {
        Transform[] listToBeReturned = new Transform[5];
        Vector3 dirFromMidToStartPoint = startPoint.transform.position - midPoint.transform.position;
        Vector3 onNormal = midPoint.transform.right;
        Vector3 projectionOfVectorAtoB = Vector3.Project(dirFromMidToStartPoint, onNormal);
        float offset = 1.5f;
        float offset1 = 1f;

        Vector3 positionOfIntersectionPoint = new Vector3();
        Vector3 positionClosestToMidPoint = new Vector3();
        Vector3 positionClosestToStart = new Vector3();

        // if angle between the two vectors is smaller than 3 degrees
        if (Vector3.Angle(onNormal, dirFromMidToStartPoint.normalized) <= 3f)
        {
            shortTurn = true;
            positionOfIntersectionPoint = midPoint.transform.position + dirFromMidToStartPoint.normalized * dirFromMidToStartPoint.magnitude / 2f;
            positionClosestToMidPoint = midPoint.transform.position + dirFromMidToStartPoint.normalized * dirFromMidToStartPoint.magnitude / 4f;
            positionClosestToStart = midPoint.transform.position + dirFromMidToStartPoint.normalized * dirFromMidToStartPoint.magnitude * 3f / 4f;
        }
        else
        {
            positionOfIntersectionPoint = midPoint.transform.position + onNormal * (projectionOfVectorAtoB.magnitude + offset);
            positionClosestToMidPoint = midPoint.transform.position + onNormal * (projectionOfVectorAtoB.magnitude / 2f);
            Vector3 dirFromIntersectionPointToStart = startPoint.transform.position - positionOfIntersectionPoint;
            positionClosestToStart = positionOfIntersectionPoint + dirFromIntersectionPointToStart.normalized * (dirFromIntersectionPointToStart.magnitude / 2f + offset) + midPoint.transform.right * offset1;
        }

        GameObject Temp1 = Instantiate(midPoint, positionClosestToMidPoint, Quaternion.identity);
        GameObject Temp2 = Instantiate(midPoint, positionOfIntersectionPoint, Quaternion.identity);
        GameObject Temp3 = Instantiate(midPoint, positionClosestToStart, Quaternion.identity);

        Temp1.transform.GetComponent<MeshRenderer>().material.color = Color.blue;
        Temp2.transform.GetComponent<MeshRenderer>().material.color = Color.blue;
        Temp3.transform.GetComponent<MeshRenderer>().material.color = Color.blue;

        listToBeReturned[0] = midPoint.transform;
        listToBeReturned[1] = Temp1.transform;
        listToBeReturned[2] = Temp2.transform;
        listToBeReturned[3] = Temp3.transform;
        listToBeReturned[4] = startPoint.transform;

        Destroy(Temp1);
        Destroy(Temp2);
        Destroy(Temp3);

        return listToBeReturned;
    }

    private static bool IsBehindMe(Transform midPoint, Vector3 startPointPosition)
    {
        Vector3 dir = startPointPosition - midPoint.position;
        return Vector3.Dot(midPoint.right, dir) < 0;
    }
    [ExecuteInEditMode]
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        if (chooseDisplay == Display.SphereReach) Gizmos.DrawWireSphere(transform.position, _sphereRadius);
        else if (chooseDisplay == Display.Splines)
            for (int i = 0; i < startTargets.Length; i++)
                for (int j = 0; j < startTargets[i].points.Length; j++) Gizmos.DrawSphere(startTargets[i].points[j], _pointRad);
    }
}
